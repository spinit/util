<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


namespace Spinit\Util\Error;

class UtilException extends \Exception {}

class MessageException extends UtilException {}

class NotFoundException extends UtilException {}
class FoundException extends UtilException {}

class NotNullNotFoundException extends NotFoundException {}
class FieldNotFoundException extends NotFoundException {}

class StopException extends UtilException {}
class StopTriggerException extends UtilException {}
