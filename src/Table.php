<?php
namespace Spinit\Util;

/**
 * Description of Table
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class Table extends Tag
{
     
    private $CurRow;
    private $CurCel;
    public function __construct()
    {
        parent::__construct('table');
    }

    public function Row($v=null)
    {
        if (!empty($v)){
            $this->CurRow->Add($v);
        } else {
            $this->CurRow = parent::Add(new Tag('tr'));
        }
        return $this->CurRow;
    }
    public function Add($o, $d = 'last')
    {
        if (!$this->CurCel) {
            $this->Cell($o);
        } else {
            $this->CurCel->Add($o, $d);
        }
        return $o;
    }
    public function AddTop($o)
    {
        if (!$this->CurCel) {
            $this->Cell($o);
        } else {
            $this->CurCel->AddTop($o);
        }
        return $o;
    }
    public function Head($val=null)
    {
        return $this->Cell($val,'th');
    }

    public function GetRow()
    {
        return $this->CurRow;
    }
    public function GetCell()
    {
        return $this->CurCel;
    }

    public function Cell($val=null, $typ='td')
    {
        if (empty($this->CurRow)){
            $this->Row();
        }
        $this->CurCel = $this->CurRow->Add(new Tag($typ));
        if ($val === '0' or !empty($val)){
            $this->CurCel->Add($val);
        }
        return $this->CurCel;
    }

    public function CellXY($val=null,$xr,$yc)
    {
        if (!is_object($this->Cnt[$xr])){
            $this->CurRow = $this->Cnt[$xr] = new Tag('tr');
        }
        $t = $this->Cnt[$xr]->Add(new Tag('td'),$yc)->Add($val);
        return $t;
    }
}