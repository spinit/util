<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;

/**
 * Description of Tag
 *
 * @author Pietro Celeste <p.celeste@spinit.it>
 */

class Tag implements \JsonSerializable
{
    use ParamTrait;
    
    private $tag = '';
    private $att = array();
    private $prp = array();
    private $cnt = array();
    private $isRemoved = false;
    private static $defaultEmptyTag = array('input','img','link', 'br', 'hr');
    private $emptyTag = null;
    private static $ref = array(); 
    private $tagdep = 0;
    private $parent = null;
    
    public function __construct($tag = '', $id = null, $emptyTag = null)
    {
        $this->setTag($tag);
        if (!empty($id)) {
            $this->att('id', $id);
        }
        $this->setEmptyTag($emptyTag);
    }
    
    public function getParent()
    {
        return $this->parent;
    }
    public function setParent($parent)
    {
        if ($this->getParent() != $parent) {
            if ($this->getParent()) {
                $this->getParent()->removeChild($this);
            }
            $this->parent = $parent;
        }
    }
    public static function getElementById($id, $notFound = null)
    {
        return arrayGet(self::$ref, $id, $notFound);
    }
    public static function create($tag, $id = null)
    {
        return new self($tag, $id);
    }
    
    public function jsonSerialize()
    {
        return trim($this->get());
    }
    public function getTag()
    {
        return $this->tag;
    }
    public function setTag($tag)
    {
        return $this->tag = $tag;
    }
    public function __get($a)
    {
        return arrayGet($this->att, $a, null);
    }
    
    public function __set($p,$v)
    {
        $this->att($p, $v);
    }
    public function removeChild($child)
    {
        if (($pos = array_search($child, $this->cnt, true)) !== false) {
            array_splice($this->cnt, $pos, 1);
        }
        return $this;
    }
    public function isRemoved()
    {
        return $this->isRemoved;
    }
    public function add($a, $d = 'last')
    {
        if (is_array($a)) {
            $item = null;
            foreach($a as $item) {
                $this->add($item, $d);
            }
            return $item;
        }
        if (is_object($a)) {
            if ($a instanceof tag) {
                $a->tagdep = abs($this->tagdep)+1;
                $this->tagdep = abs($this->tagdep) * -1;
            }
            
            if ($a->id && array_key_exists($a->id, self::$ref)) {
                $a = self::$ref[$a->id];
            } elseif ($a->id) {
                self::$ref[$a->id] = $a;
            }
            $a->setParent($this);
        }
        if ($d=='last') {
            if (is_array($this->cnt)) {
                array_push($this->cnt, $a);
            } 
        } else {
            array_unshift($this->cnt, $a);
            ksort($this->cnt);
        }
        return $a;
    }
    public function prp($val) {
        if (!in_array($val, $this->prp)) {
            $this->prp[] = $val;
        }
        return $this;
    }
    public function att($p, $v='', $concat=false)
    {
        if (is_array($p)) {
            foreach ($p as $k => $v) {
                $this->att($k, $v);
            }
            return $this;
        } 
        // era già registrato?
        if ($p == 'id') {
            if ($oldid = arrayGet($this->att, 'id')) {
                unset(self::$ref[$oldid]);
            }
            self::$ref[$v] = $this;
        }
        if ($concat && !empty($this->att[$p])) {
            $concat_car = ($concat===true) ? ' ' : $concat;
            $this->att[$p] .= "{$concat_car}{$v}";
        } else {
            $this->att[$p] = $v;
        }
        return $this;
    }
    public function inner($withSpace = true)
    {
        if ($this->isRemoved) {
            return '';
        }
        $strContent = '';
        foreach ($this->cnt as $content) {
            if ($content instanceof self) {
                $content = $content->build($withSpace);
            }
            $strContent .= $content;
        }
        return $strContent;
    }
    
    protected function build($withSpace = true)
    {
        $spaces = '';
        $strContent = $this->inner($withSpace);
        if ($this->isRemoved) {
            return '';
        }
        $tag = $this->tag;
        if ($tag == '') {
            return $strContent;
        }
        $strTag = '';
        if (!empty($tag)){
            if ($withSpace) {
                $spaces = $this->tagdep != 0 ? "\n".str_repeat("  ",abs($this->tagdep)) : '';
            }
            $strTag = $spaces.'<'.$tag;
            foreach ($this->att as $key => $val) {
                if (is_array($val) or is_object($val)) {
                    $val = json_encode($val, JSON_UNESCAPED_UNICODE);
                }
                $strTag .= ' '.$key.'="'.htmlspecialchars($val, ENT_QUOTES).'"';
            }
            foreach ($this->prp as $val) {
                if (is_array($val) or is_object($val)) {
                    $val = '<!-- '.json_encode($val, JSON_UNESCAPED_UNICODE).' --> ';
                }
                $strTag .= ' '.$val;
            }
        }
        if ($this->isEmptyTag($tag)) {
            $strTag .= '/>';
        } else {
            $strTag .= '>';
            $spaces2 = $this->tagdep < 0 ? $spaces : '';
            $strTag .= $strContent . (!empty($tag) ? $spaces2."</{$tag}>" : '');
        }
        return $strTag;
    }
    public function isEmptyTag($tag)
    {
        $emptyTag = $this->emptyTag ? $this->emptyTag : self::$defaultEmptyTag;
        return in_array($tag, $emptyTag);
    }
    public function setEmptyTag($emptyTag)
    {
        $this->emptyTag = $emptyTag;
    }
    
    public function get($withSpace = true)
    {
        return $this->build($withSpace);
    }
    
    public function child($i=0)
    {
        $args = func_get_args();
        if (count($args)==0) {
            return $this->cnt;   
        }
        return arrayGet($this->cnt, array_shift($args), false);
    }
    
    public function isEmpty()
    {
        $count = 0;
        foreach($this->cnt as $a) {
            if ($a instanceof self) {
                if (!$a->isRemoved()) {
                    $count += 1;
                }
            } else {
                $count += 1;
            }
            if ($count) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * 
     * @return string
     * @codeCoverageIgnore
     */
    public function __toString()
    {
        try {
            return $this->get();
        } catch (\Exception $e) {
            trigger_error($e->getMessage());
            return debug_backtrace(10);
        }
    }
    
    public function set()
    {
        $this->cnt = array();
        foreach(func_get_args() as $c) {
            $this->add($c);
        }
        return $this;
    }
    
    public function remove($isRemoved = true) 
    {
        $this->isRemoved = $isRemoved;
    }
}
