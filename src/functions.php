<?php

/* 
 * Copyright (C) 2017 Ermanno Astolfi <ermanno.astolfi@spinit.it>.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 */

namespace Spinit\Util;

use Webmozart\Assert\Assert;


/**
 * Restituisce l'elemento dell'array se presente ... altrimenti default
 * @param type $array
 * @param type $field
 * @param type $default
 * @return type
 */
function arrayGet($array, $fields, $default = '', $notEmpty = true)
{
    // permette di ritornare un valore o il risultato di una funzione
    $retDefault = function ($val) {
        // per essere eseguibile occorre sia una chiusura e non un nome di una funzione
        if (is_callable($val) and !is_string($val)) {
            return $val();
        }
        return $val;
    };
    if ($fields === null) {
        $fields = '';
    }
    // si possono indicare una serie di campi in cascata che vengono verificati in sequenza
    /*
    if (!is_array($fields)) {
        $fields = [$fields];
    }
    */
    // avvio ispezione
    $value = $array;
    foreach(asArray($fields, '.')?:[''] as $field) {
        if (!is_string($field) and ! is_integer($field)) {
            throw new \Exception('Invalid field type : '.json_encode($field));
        }
        // verifica sulla possibilità di poter ispezionare l'array
        $is_array = (is_array($value) or ($value instanceof \ArrayAccess));
        // l'insieme da analizzare ... non è un insieme
        if (!$is_array) {
            return $retDefault($default);
        }
        // non contiene il valore indicato
        if (is_array($value)) {
            if (!array_key_exists($field, $value)) {
                return $retDefault($default);
            }
        } else {
            if (!$value->offsetExists($field)) {
                return $retDefault($default);
            }
        }
        // contiene il valore ma è vuoto ... e non lo si vuole vuoto
        if ($notEmpty and !$value[$field] and $value[$field]!='0') {
            return $retDefault($default);
        }
        $value = $value[$field];
    }
    return $value;
}


/**
 * Restituisce l'elemento dell'array se presente ... altrimenti Exception
 * @param type $array
 * @param type $field
 * @param type $default
 * @return type
 */
function arrayGetAssert($array, $field, $message = '', $code = 0)
{
    $message = sprintf(nvl($message, 'Campo non trovato : %s'), is_string($field) ? $field : json_encode($field));
    return arrayGet($array, $field, throwNotFound($message, $code), false);
}

/**
 * Individua la funzione memorizzata nell'array e la esegue. Se non la trovao esegue $notFound
 * @param type $array
 * @param type $method
 * @param type $params
 * @param type $notFound
 * @return type
 */
function arrayExec($array, $method, $params = array(), $notFound = null)
{
    // se come terzo parametro viene impostata la callback allora gli argomenti della funzione sono []
    if (is_callable($params)) {
        $notFound = $params;
        $params = array();
    }
    $notFound = nvl($notFound, throwNotFound("metodo non trovato : ".$method));
    $callable = arrayGet($array, $method, $notFound, false);
    if (!is_callable ($callable)) {
        return $notFound();
    }
    return call_user_func_array($callable, $params);
}

/**
 * Restituisce il primo elemento non null ... altrimenti stringa vuota
 * @param type $array
 * @param type $field
 * @param type $default
 * @return type
 */

function nvl()
{
    $args = func_get_args();
    do {
        $a = array_shift($args);
        if ($a or (is_string($a) and strlen($a))) {
            return $a;
        }
    } while(count($args));
    return $a;
}

/**
 * Restituisce il primo elemento non null ... altrimenti Exception
 * @param type $array
 * @param type $field
 * @param type $default
 * @return type
 */

function nvlAssert()
{
    $args = func_get_args();
    do {
        $a = array_shift($args);
        if ($a) {
            return $a;
        }
    } while(count($args));
    throw new Error\NotFoundException();
}

/**
 * Trasforma $prefix in un array se è una stringa utilizzando $separator come separatore.
 * Se suffix è presente lo accoda al risultato
 * @param type $prefix
 * @param type $separator
 * @param type $suffix
 */
function asArray($prefix, $separator = '', $suffix = array())
{
    if (!is_array($suffix)) {
        $suffix = asArray($suffix, $separator);
    }
    if (!is_array($prefix)) {
        if ($separator) {
            $prefix = array_filter(array_map('trim', explode($separator, $prefix)), 'strlen');
        } else {
            if (!$prefix) {
                $prefix = [];
            } else if (is_string($prefix)) {
                $prefix = array_filter(array_map('trim', [$prefix]), 'strlen');
            } else {
                $prefix = [$prefix];
            }
        }
    }
    foreach($suffix as $add) {
        $prefix[] = $add;
    }
    return $prefix;
}

/**
 * Determina il path della classe passata eventualmente con la struttare n1:n2:cname
 * @param type $class
 * @return type
 */
function getClassPath($class)
{
    return '\\'.trim(str_replace(':','\\', trim($class, ':')),'\\');
}
function getColonsPath($class) 
{
    return trim(str_replace('\\', ':', $class), ':');
}
/**
 * Instanzia una classe usando il primo parametro come factory e i rimanenti come argomenti
 */
function getInstance()
{
    $args = func_get_args();
    $class = array_shift($args);
    
    if (is_string($class)) {
        $part = asArray($class, '.');
        $classPath = getClassPath(array_shift($part));
        $method = nvl(array_shift($part), '__getInstance__');
        $reflect  = new \ReflectionClass($classPath);
        try {
            if ($reflect->getMethod($method)->isStatic()) {
                return call_user_func_array($classPath.'::'.$method, $args);
            }
        } catch (\Exception $e) {
        }
        $instance = $reflect->newInstanceArgs($args);
    } else if (is_callable ($class)) {
        $instance = call_user_func_array($class, $args);
    } else {
        Assert::methodExists($class, '__getInstance__');
        $instance = call_user_func_array(array($class, '__getInstance__'), $args);
    }
    return $instance;
}

function getInstanceArray() {
    $args = $argsList = func_get_args();
    if (count($argsList)>1) {
        $args = array_pop($argsList);
        if (!is_array($args)) {
            $args = [$args];
        }
        $args = array_merge($argsList, $args);
    }
    return call_user_func_array(__NAMESPACE__.'\\getInstance', $args);
}

/**
 * Normalizza un nome in un array che abbia le componenti minime attese
 * @param string|array $name
 * @param string $module
 * @param string $sep
 * @return array
 * @throws Error\NotFoundException
 */
function biName($name, $module = 'Core', $sep = ':')
{
    $arname = asArray($name, $sep);
    switch(count($arname))
    {
        case 0:
            return null;
        case 1:
            array_unshift($arname, $module);
            break;
    }
    return $arname;
}

/**
 * Opera come dirname ma su stringhe strutturate con qualsiasi separatore
 * @param type $item
 * @param type $sep
 * @param type $count
 * @return type
 */
function upName($item, $sep = '\\', $count = 1)
{
    if ($count > 1) {
        $item = upName($item, $sep, $count -1);
    }
    $struct = explode($sep, $item);
    array_pop($struct);
    return implode($sep, $struct);
}

/**
 * Prepara una chiusura che genera un Errore (o la classe passata come secondo parametro) se richiamata
 * @param type $message
 * @param type $class
 * @return type
 */
function throwException($params, $class = '\\Exception')
{
    if (!is_array($params)) {
        $params = array($params);
    }
    $message = array_shift($params);
    $code = nvl(array_shift($params), 0);
    return function () use ($message, $code, $class) {
        throw getInstance($class, $message, $code);
    };
}
function throwError($name, $message, $code = 0)
{
    return throwException([$message, $code], __NAMESPACE__.':Error:'.$name);
}
function throwNotFound($message, $code = 0)
{
    return throwError('NotFoundException', $message, $code);
}


/**
 * Modifica il carattere '/' nel DIRECTORY_SEPARATOR della macchina
 * @param type $url
 * @return type
 */
function sanitizePath($path)
{
    return str_replace('/', DIRECTORY_SEPARATOR, $path);
}

/**
 * Permette di prelevare il contenuto di un file senza preoccuparsi del DIRECTORY_SEPARATOR della macchina su cui gira.
 * L'importante è usare sempre '/' nella costruzione dei path
 * @return type
 */
function file_get_contents()
{
    $args = func_get_args();
    // se non è una url effettuo al conversione
    if (strpos($args[0], '://') === false) {
        $args[0] = sanitizePath($args[0]);
    }
    return call_user_func_array('\file_get_contents', $args);
}

/**
 * Ritorna la variabile di ambiente .. e permette di modificarne il valore internamente
 * @staticvar array $ENV
 * @return any
 */
function getenv()
{
    static $ENV = array();
    $args = func_get_args();
    switch(count($args)) {
        case 0:
            return $ENV;
        case 1:
            if (isset($ENV[$args[0]])) {
                return $ENV[$args[0]];
            }
            return \getenv($args[0]);
        default:
            $ENV[$args[0]] = $args[1];
            return $args[1];
    }
}

/**
 * Funzione di debuging
 * @return type
 */
function console()
{
    if (!getenv('SPINIT_UTIL_CONSOLE')) { return true;}
    try {
        $fname = str_replace('[date]',\date('Ymd'), getenv('SPINIT_UTIL_CONSOLE'));
        $fp = \fopen($fname, 'a');

        $args = func_get_args();
        $title = '';
        if (!is_array($args[0]) and !is_object($args[0])) {
            $title = ' == '.\array_shift($args);
        }
        $caller = arrayGet(\debug_backtrace(), 0);
        $class = arrayGet($caller, 'class', arrayGet($caller, 'file'));
        $line = arrayGet($caller, 'line');
        
        \fwrite($fp, "== ".date('Y-m-d H:i:s').$title." [{$class}:{$line}]\n");
        while($arg = \array_shift($args)) {
            try {
                \fwrite($fp, print_r($arg, 1)."\n");
            } catch (\Exception $e) {
                \fwrite($fp, "@@ ERROR @@ : ".$e->getMessage()."\n");
            }
        }
        \fclose($fp);
    } catch (\Exception $e) {
        return false;
    }
    return $fname;
}


function getContent($phpFile, $arguments = array())
{
    ob_start();
    include($phpFile);
    return ob_get_clean();
}


function rebase($num, $coder = '')
{
    $res = '';
    $coder = $coder?:"0123456789abcdefghijklmnopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVXYZ";
    $mod = $num % strlen($coder);
    while($num = intval($num / strlen($coder))) {
        $res = $coder{$mod}.$res;
        $mod = $num % strlen($coder);
    }
    $res = $coder{$mod}.$res;
    return $res;
}

function parseConfig($config)
{
    $param = [];
    foreach(asArray($config, "\n") as $line) {
        if (!trim($line)) {
            continue;
        }
        $ar = asArray($line, ':');
        $param[strtolower(trim(array_shift($ar)))] = trim(implode(':', $ar));
    }
    return $param;

}

function parseHttpResponse ($string) 
{

    $headers = array();
    $content = '';
    $h = null;
    $code = '';
    // ricerca  code + headers
    foreach(explode("\n", $string) as $str) {
        if ($h !== false and substr($str, 0, strlen('HTTP/1')) == 'HTTP/1') {
            $part = explode(' ',$str);
            $code = $part[1];
            $str = strtok("\n");
            continue;
        }
        if ($h!== false and trim($str) === '') {                
            $h = false;
            $str = strtok("\n");
            continue;
        }
        if ($h !== false and false !== strpos($str, ':')) {
            $h = true;
            list($headername, $headervalue) = explode(':', trim($str), 2);
            $headername = strtolower($headername);
            $headervalue = ltrim($headervalue);
            if (isset($headers[$headername])) 
                $headers[$headername] .= ',' . $headervalue;
            else 
                $headers[$headername] = $headervalue;
        }
        if ($h === false) {
            $content .= $str."\n";
        }
        $str = strtok("\n");
    }
    // parsing contenuto
    $content = trim($content);
    if (isset($headers['content-type'])) {
        $app1 = 'application/json';
        $app2 = 'text/xml';
        if (substr($headers['content-type'], 0, strlen($app1)) == $app1) {
            $headers['type'] = 'json';
            $content = json_decode($content, 1);
        } else if (substr($headers['content-type'], 0, strlen($app2)) == $app2) {
            $headers['type'] = 'xml';
            $content = simplexml_load_string($content, null, LIBXML_NOCDATA);
        }
    }
    return array($code, $headers, $content);
}

/**
 * Verifica l'esistenza e l'usabilità di una directory
 * 
 * @param type $dir
 * @return type
 * @throws \Exception
 */
function checkDir($dir)
{
    if (!is_dir($dir) and !@mkdir($dir, 0777, true)) {
        throw new \Exception('Directory not found : '.$dir);
    }
    if (!is_writable($dir)) {
        throw new \Exception('Directory not writable : '.$dir);
    }
    return $dir;
}

function arraySubset($array, $fields)
{
    $ret = [];
    foreach(asArray($fields) as $f) {
        $ret[$f] = arrayGet($array, $f);
    }
    return $ret;
}

function normalize($cnt, $params=array(), $args = array(), $info = array(), $vars = array())
{
    $debug = 0;
    if (is_array($cnt)) {
       $sql = array_shift($cnt);
       $fnc = array_shift($cnt);
       $debug = array_shift($cnt);
    } else {
        $sql = $cnt;
        $fnc = function($field, $params, $matches, $k, $sql) {
            switch($field{0}) {
                case '*':
                    $field = substr($field, 1);
                    break;
                case ':':
                    $field = substr($field, 1);
                    break;
                case '+':
                    $field = substr($field, 1);
                    break;
                case '@':
                    $field = substr($field, 1);
                    break;
            }
            // ritorna la nuova stringa piú la lista dei parametri sostituiti
            return [str_replace($matches[$k], arrayGet($params, explode('.', $field)), $sql), []];
        };
    }
    $is_array = (is_array($params) or ($params instanceof \ArrayAccess));
    if (!$is_array) {
        $params = ['id' => $params];
    }
    $merger = function($path, &$pp) use (&$sql, $fnc) {
        preg_match_all($path, $sql, $matches);
        $unsetList = [];
        foreach ($matches[1] as $k => $name) {
            list($sql, $unset) = call_user_func($fnc, $name, $pp, $matches[0], $k, $sql);
            foreach ($unset as $v ) {
                $unsetList[] = $v;
            }
        }
        foreach($unsetList as $v) {
            unset($pp[$v]);
        }
    };
    // sostituzione parametri nella query
    $merger('/\{\{([^\}]+)\}\}/', $params);
    $merger('/\{\[([^\]]+)\]\}/', $args);
    $merger('/\{\(([^\)]+)\)\}/', $info);
    $merger('/\{\|([^\|]+)\|\}/', $vars);
    return [$sql, $params];
}

namespace Spinit\Util\Log;

function notice($message)
{
    return \trigger_error($message, E_USER_NOTICE);
}

function warning($message)
{
    return \trigger_error($message, E_USER_WARNING);
}

function error($message)
{
    return \trigger_error($message, E_USER_ERROR);
}
