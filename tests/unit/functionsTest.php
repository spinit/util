<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;
use Spinit\Util\Error;

/**
 * Description of functionTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class functionTest extends \PHPUnit_Framework_TestCase
{
    public function testArrayGet()
    {
        $object = array('uno'=>'1', 'due'=>'2', 'quattro'=>'');
        $this->assertEquals('1', arrayGet($object, 'uno'));
        $this->assertEquals('2', arrayGet($object, 'due'));
        $this->assertEquals('', arrayGet($object, 'tre'));
        $this->assertEquals('3', arrayGet($object, 'tre', '3', false));
        $this->assertEquals('4', arrayGet($object, 'quattro','4'));
        $this->assertEquals('', arrayGet($object, 'quattro', '4', false));
        $message = 'ok test';
        $this->assertEquals('ok test', arrayGet($object, 'tre', function() use ($message) {return $message;}));
    }
    
    public function testMultiArrayGet()
    {
        $object = array('uno'=>['due'=>'2', 'quattro'=>'']);
        $this->assertEquals('2', arrayGet($object, ['uno', 'due']));
        $this->assertEquals('', arrayGet($object, ['uno', 'tre']));
        $this->assertEquals('3', arrayGet($object, ['uno', 'tre'], '3', false));
        $this->assertEquals('4', arrayGet($object, ['uno', 'quattro'],'4'));
        $this->assertEquals('', arrayGet($object, ['uno', 'quattro'], '4', false));
    }
    
    /**
     * @expectedException Spinit\Util\Error\NotFoundException
     * @expectedExceptionMessage Message Test
     * @expectedExceptionCode 100
     */
    public function testArrayGetAssert()
    {
        $object = array('uno'=>'1', 'due'=>'2');
        $this->assertEquals('1', arrayGetAssert($object, 'uno'));
        $this->assertEquals('2', arrayGetAssert($object, 'due'));
        arrayGetAssert($object, 'tre', 'Message Test', 100);
    }
    
    public function testNvl()
    {
        $this->assertEquals('uno', nvl('uno','due'));
        $this->assertEquals('due', nvl('', 'due'));
        $this->assertEquals('', nvl());
        $this->assertEquals('', nvl(false, null));
    }
    
    /**
     * @expectedException Spinit\Util\Error\NotFoundException
     */
    public function testNvlAssert()
    {
        $this->assertEquals('uno', nvlAssert('uno','due'));
        $this->assertEquals('due', nvlAssert('', 'due'));
        nvlAssert(false, null);
    }
    
    public function testAsArray()
    {
        $this->assertEquals(array('1', '2', '3'), asArray('1,2',',','3'));
        $this->assertEquals(array('1', '2', '2.1', '3', '4'), asArray('1,  2,2.1',',','3, 4'));
    }
    
    public function testGetInstance()
    {
        //$this->assertEquals('hello->world', getInstance($this, 'hello', 'world'));
        $this->assertEquals('hello.world', getInstance(function($a, $b) {return $a.'.'.$b;}, 'hello', 'world'));
        //$this->assertEquals('hello=world', (string) getInstance(__NAMESPACE__.'\\TestUtil\TestInst', 'hello', 'world'));
    }
    public function getInstance($par1, $par2)
    {
        return $par1.'->'.$par2;
    }
    
    public function testBiName()
    {
        $this->assertEquals(array('uno','due'), biName('uno:due'));
        $this->assertEquals(array('Core','due'), biName('due'));
        $this->assertEquals(array('uno','due', 'tre'), biName('uno,due,tre','',','));
        $this->assertNull(biName(':'));
    }
    
    public function testUpName()
    {
        $this->assertEquals("uno:due", upName("uno:due:tre", ':'));
        $this->assertEquals("uno:due", upName("uno:due:tre:qu:at:tro", ':', 4));
    }
    /**
     * @expectedException \Exception
     * @expectedExceptionMessage OK TEST
     */
    public function testErrorAsync()
    {
        $callback = throwException('OK TEST');
        $callback();
    }
    
    public function testArrayExec()
    {
        $array = array('test'=> function($cc){ return 'OK : '.$cc;});
        $this->assertEquals('OK : 1', arrayExec($array, 'test', array('1')));
        $this->assertEquals('OK', arrayExec($array, 'bad', function(){return 'OK';}));
    }
    
    public function testGetEnv()
    {
        $varname = 'VARIABILE_DI_AMBIENTE_CHE_NON_ESISTE_'.md5(__FILE__);
        $this->assertEquals(null, getenv($varname));
        $this->assertEquals('5', getenv($varname, '5'));
        $this->assertEquals('5', getenv($varname));
        $this->assertEquals(array($varname=>'5'), getenv());
    }
    
    public function testConsole()
    {
        getenv('SPINIT_UTIL_CONSOLE',null);
        $this->assertEquals(true, console('hello word'));
        getenv('SPINIT_UTIL_CONSOLE','/');
        $this->assertEquals(false, console('hello word'));
        $fname = '/tmp/[date]'.md5(__FILE__).'-test.log';
        getenv('SPINIT_UTIL_CONSOLE',$fname);
        $expected = str_replace('[date]', date('Ymd'), $fname);
        $this->assertEquals($expected, console('hello word'));
        $this->assertContains('hello word',file_get_contents($expected));
        $this->assertEquals($expected, console('hello',array('word')));
        $this->assertContains('word',file_get_contents($expected));
    }
    
    public function testRebase()
    {
        $this->assertEquals("1", rebase('1', "0123"));
        $this->assertEquals("2", rebase('2', "0123"));
        $this->assertEquals("3", rebase('3', "0123"));
        $this->assertEquals("10", rebase('4', "0123"));
        $this->assertEquals("1", rebase('1'));
        $this->assertEquals("k", rebase('20'));
        $this->assertEquals("P", rebase('50'));
        $this->assertEquals("1F", rebase('100'));
        $this->assertEquals("1G", rebase('101'));
    }
    
    public function testParseConfig()
    {
        $this->assertEquals([], parseConfig(""));
        $this->assertEquals(['a'=>''], parseConfig("a"));
        $this->assertEquals(['a'=>''], parseConfig("a:"));
        $this->assertEquals(['a'=>'1'], parseConfig("a:1"));
        $this->assertEquals(['a'=>'1', 'b'=>''], parseConfig("a:1\n\nb"));
        $this->assertEquals(['a'=>'1', 'b'=>'2'], parseConfig("a:1\n\nb:2"));
    }
    public function testParseHttpResponseJson()
    {
        $string = <<<EOCNT
HTTP/1.1 200 OK
Server: nginx/1.14.0
Date: Fri, 11 Jan 2019 10:24:02 GMT
Content-Type: application/json; charset=utf-8
Content-Length: 91
Connection: keep-alive
Cache-Control: no-cache
Pragma: no-cache
Expires: -1

{"uploadFileName":"testFile.xml","errorCode":"0000","errorDescription":null}
EOCNT;
        $response = parseHttpResponse($string);
        $this->assertEquals('200', $response[0]);
        $this->assertEquals('no-cache', $response[1]['pragma']);
        $this->assertEquals('json', $response[1]['type']);
        $this->assertEquals('testFile.xml', $response[2]['uploadFileName']);
    }
    public function testParseHttpResponseXml()
    {
        $string = <<<EOCNT
HTTP/1.1 200 OK
Server: nginx/1.14.0
Date: Fri, 11 Jan 2019 10:24:02 GMT
Content-Type: text/xml; charset=utf-8
Content-Length: 91
Connection: keep-alive
Cache-Control: no-cache
Pragma: no-cache
Expires: -1

<a att="rib"><b>uno</b><c>test</c></a>
EOCNT;
        $response = parseHttpResponse($string);
        $this->assertEquals('200', $response[0]);
        $this->assertEquals('xml', $response[1]['type']);
        $this->assertEquals('uno', (string)$response[2]->b);
        $this->assertEquals('test', (string)$response[2]->c);
        $this->assertEquals('rib', (string)$response[2]['att']);
    }
}

namespace Spinit\Util\TestUtil;

class TestInst
{
    private $message;
    public function __construct($a, $b) {
        $this->message = $a.'='.$b;
    }
    public function __toString() {
        return $this->message;
    }
}
