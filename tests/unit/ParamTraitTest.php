<?php
namespace Spinit\Util\UnitTest;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Spinit\Util\ParamInterface;
use PHPUnit\Framework\TestCase;


/**
 * Description of ExecCodeTraitTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class ParamTraitTest extends TestCase implements ParamInterface
{
    use \Spinit\Util\ParamTrait;
    
    /**
     * @expectedException Spinit\Util\Error\NotFoundException
     */
    public function testParam()
    {
        $this->setParam('hello', 'word')
            ->setParam('test','prova')
            ->setParam('empty', '');
        
        $this->assertEquals('word', $this->getParam('hello'));
        $this->assertEquals('prova', $this->getParam('test'));
        
        $this->assertEquals('', $this->getParam('notSet'));
        $this->assertEquals('hello', $this->getParam('notSet', 'hello'));
        
        $this->assertEquals('', $this->getParam('empty'));
        $this->assertEquals('', $this->getParam('empty', 'OK'));
        $this->assertEquals('OK', $this->getParam('empty', 'OK', true));
        
        $this->assertEquals('', $this->getParam('vuoto'));
        $this->assertEquals('OK', $this->getParam('vuoto', 'OK'));
        $this->getParam('vuoto','OK', true);
    }
}
