<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Spinit\Util;

use PHPUnit\Framework\TestCase;

/**
 * Description of DictionaryTest
 *
 * @author Ermanno Astolfi <ermanno.astolfi@spinit.it>
 */
class DictionaryTest extends TestCase
{
    public function testEmpty()
    {
        $this->object = new Dictionary();
        
        $this->object['uno'] = '1';
        $this->assertEquals('1', $this->object['uno']);
    }
    public function testSet()
    {
        $this->object = new Dictionary(array('uno'=>1));
        $this->assertEquals('1', $this->object['uno']);
    }
    public function testSelf()
    {
        $test = new Dictionary(array('uno'=>1));
        $this->object = new Dictionary($test);
        $this->assertEquals('1', $this->object['uno']);
        $this->assertTrue(isset($test['uno']));
        unset($test['uno']);
        $this->assertFalse(isset($test['uno']));
    }
    public function testCount()
    {
        $test = new Dictionary(array('uno'=>1));
        $this->assertEquals(1, count($test));
        $test['due'] = 'tre';
        $this->assertEquals(2, count($test));
        $this->assertEquals('1', $test->shift());
        $this->assertEquals(1, count($test));
        $this->assertEquals('tre', $test['due']);
        $test->unshift('ciao');
        $this->assertEquals(2, count($test));
        $this->assertEquals('ciao', $test[0]);
        $test[] = 'sette';
        $this->assertEquals(3, count($test));
    }
    public function testInvoke()
    {
        $test = new Dictionary(array('due'=>'tre'));
        $this->assertEquals('tre', $test('due'));
        $this->assertEquals(json_encode(array('due'=>'tre')), json_encode($test));
    }
    public function testMerge()
    {
        $test = new Dictionary();
        $test->merge('due.quattro', array('uno'=>'2'));
        $this->assertEquals(array('due'=>array('quattro'=>array('uno'=>'2'))), $test->asArray());
        $this->assertEquals('2', $test('due.quattro.uno'));
        $this->assertFalse($test->keyExists('due.cinque'));
        $test->merge('due.cinque', array('uno'=>'3'));
        $this->assertTrue($test->keyExists('due.cinque'));
        $this->assertEquals('3', $test('due.cinque.uno'));
        $test->merge('due', 'uno');
        $this->assertEquals(array('uno'=>'2'), $test('due.quattro'));
    }
    
    public function testIterator()
    {
        $test = new Dictionary();
        $test[] = 1;
        $test[] = 2;
        $test[] = 3;
        foreach($test as $key => $value) {
            $this->assertEquals($value, $key+1);
        }
    }
    
    public function testAppend()
    {
        $test = new Dictionary();
        $test->append('uno', 'due', 'tre');
        $this->assertEquals(array('tre'), $test('uno.due'));
    }
    
    /**
     * @expectedException \Exception
     */
    public function testCallable()
    {
        $test = new Dictionary();
        $b = 'hello';
        $test['saluta'] = function($a) use ($b) {
            return $b.' '.$a;
        };
        $this->assertEquals('hello world', $test->saluta('world'));
        $test->nonSaluta('');
    }
    
    public function testTrigger()
    {
        $test = new Dictionary();
        $test->bindBefore('test', function($event) {
            $event['result'] = $event['when'];
        });
        $test->bindExec('test', function($event) {
            $event['result'] = $event['when'];
        });
        $test->bindAfter('test', function($event) {
            $event->stopPropagation();
            $event['result'] = $event['when'];
        });
        $test->bindEnd('prova', function($event) {
            $event['result'] = $event['when'];
        });
        
        $result = $test->trigger('test');
        $this->assertEquals('exec', $result);
        
        $result = $test->trigger('prova');
        $this->assertEquals('end', $result);
    }
    
    public function testDephEvent()
    {
        $test = new Dictionary();
        $test->bindBefore(['this', 'is', 'a', 'test'], function($event) {
            return 'OK';
        });
        $result = $test->trigger(['this', 'is','a']);
        $this->assertTrue(is_array($result));
        $result = $test->trigger(['this', 'is','a', 'test']);
        $this->assertEquals('OK', $result);
    }
}
